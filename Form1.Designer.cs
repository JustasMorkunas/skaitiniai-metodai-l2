﻿namespace PracticalGuideCharts
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PlotPanel = new System.Windows.Forms.Panel();
            this.trkElevation = new System.Windows.Forms.TrackBar();
            this.trkAzimuth = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbElevation = new System.Windows.Forms.TextBox();
            this.tbAzimuth = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.radioZ1 = new System.Windows.Forms.RadioButton();
            this.radioZ2 = new System.Windows.Forms.RadioButton();
            this.button12 = new System.Windows.Forms.Button();
            this.U21 = new System.Windows.Forms.RadioButton();
            this.U22 = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.trkElevation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkAzimuth)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // PlotPanel
            // 
            this.PlotPanel.Location = new System.Drawing.Point(16, 49);
            this.PlotPanel.Margin = new System.Windows.Forms.Padding(4);
            this.PlotPanel.Name = "PlotPanel";
            this.PlotPanel.Size = new System.Drawing.Size(709, 462);
            this.PlotPanel.TabIndex = 0;
            // 
            // trkElevation
            // 
            this.trkElevation.Location = new System.Drawing.Point(16, 519);
            this.trkElevation.Margin = new System.Windows.Forms.Padding(4);
            this.trkElevation.Maximum = 90;
            this.trkElevation.Minimum = -90;
            this.trkElevation.Name = "trkElevation";
            this.trkElevation.Size = new System.Drawing.Size(468, 56);
            this.trkElevation.TabIndex = 0;
            this.trkElevation.TickFrequency = 5;
            this.trkElevation.Value = 30;
            this.trkElevation.Scroll += new System.EventHandler(this.trkElevation_Scroll);
            // 
            // trkAzimuth
            // 
            this.trkAzimuth.Location = new System.Drawing.Point(16, 583);
            this.trkAzimuth.Margin = new System.Windows.Forms.Padding(4);
            this.trkAzimuth.Maximum = 180;
            this.trkAzimuth.Minimum = -180;
            this.trkAzimuth.Name = "trkAzimuth";
            this.trkAzimuth.Size = new System.Drawing.Size(468, 56);
            this.trkAzimuth.TabIndex = 1;
            this.trkAzimuth.TickFrequency = 10;
            this.trkAzimuth.Value = -37;
            this.trkAzimuth.Scroll += new System.EventHandler(this.trkAzimuth_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(556, 519);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Elevation (degree)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(556, 583);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Azimuth (degree)";
            // 
            // tbElevation
            // 
            this.tbElevation.Location = new System.Drawing.Point(558, 540);
            this.tbElevation.Margin = new System.Windows.Forms.Padding(4);
            this.tbElevation.Name = "tbElevation";
            this.tbElevation.Size = new System.Drawing.Size(96, 22);
            this.tbElevation.TabIndex = 4;
            this.tbElevation.Text = "30";
            this.tbElevation.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbElevation_KeyUp);
            // 
            // tbAzimuth
            // 
            this.tbAzimuth.Location = new System.Drawing.Point(562, 604);
            this.tbAzimuth.Margin = new System.Windows.Forms.Padding(4);
            this.tbAzimuth.Name = "tbAzimuth";
            this.tbAzimuth.Size = new System.Drawing.Size(92, 22);
            this.tbAzimuth.TabIndex = 5;
            this.tbAzimuth.Text = "-37";
            this.tbAzimuth.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbAzimuth_KeyUp);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(838, 49);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(157, 28);
            this.button1.TabIndex = 6;
            this.button1.Text = "Bar3D";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(838, 85);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(157, 28);
            this.button2.TabIndex = 7;
            this.button2.Text = "Contour";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(838, 873);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(157, 28);
            this.button3.TabIndex = 8;
            this.button3.Text = "Close";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(838, 121);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(157, 28);
            this.button4.TabIndex = 9;
            this.button4.Text = "FillContour";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.Location = new System.Drawing.Point(838, 191);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(157, 28);
            this.button5.TabIndex = 12;
            this.button5.Text = "MeshContour";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.Location = new System.Drawing.Point(838, 155);
            this.button6.Margin = new System.Windows.Forms.Padding(4);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(157, 28);
            this.button6.TabIndex = 11;
            this.button6.Text = "Mesh";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.Location = new System.Drawing.Point(838, 367);
            this.button8.Margin = new System.Windows.Forms.Padding(4);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(157, 28);
            this.button8.TabIndex = 18;
            this.button8.Text = "XYColor";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button9.Location = new System.Drawing.Point(838, 331);
            this.button9.Margin = new System.Windows.Forms.Padding(4);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(157, 28);
            this.button9.TabIndex = 17;
            this.button9.Text = "Waterfall";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button10.Location = new System.Drawing.Point(838, 295);
            this.button10.Margin = new System.Windows.Forms.Padding(4);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(157, 28);
            this.button10.TabIndex = 16;
            this.button10.Text = "SurfaceContour";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button11.Location = new System.Drawing.Point(838, 260);
            this.button11.Margin = new System.Windows.Forms.Padding(4);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(157, 28);
            this.button11.TabIndex = 15;
            this.button11.Text = "Surface";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button13
            // 
            this.button13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button13.Location = new System.Drawing.Point(838, 226);
            this.button13.Margin = new System.Windows.Forms.Padding(4);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(157, 28);
            this.button13.TabIndex = 13;
            this.button13.Text = "MeshZ";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(835, 493);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "L2 užduotys";
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button7.Location = new System.Drawing.Point(838, 513);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(157, 23);
            this.button7.TabIndex = 20;
            this.button7.Text = "Gauso metodas";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(16, 646);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(709, 255);
            this.richTextBox1.TabIndex = 21;
            this.richTextBox1.Text = "";
            // 
            // radioZ1
            // 
            this.radioZ1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioZ1.AutoSize = true;
            this.radioZ1.Checked = true;
            this.radioZ1.Location = new System.Drawing.Point(838, 415);
            this.radioZ1.Name = "radioZ1";
            this.radioZ1.Size = new System.Drawing.Size(46, 21);
            this.radioZ1.TabIndex = 22;
            this.radioZ1.TabStop = true;
            this.radioZ1.Text = "Z1";
            this.radioZ1.UseVisualStyleBackColor = true;
            // 
            // radioZ2
            // 
            this.radioZ2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioZ2.AutoSize = true;
            this.radioZ2.Location = new System.Drawing.Point(838, 443);
            this.radioZ2.Name = "radioZ2";
            this.radioZ2.Size = new System.Drawing.Size(46, 21);
            this.radioZ2.TabIndex = 23;
            this.radioZ2.Text = "Z2";
            this.radioZ2.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button12.Location = new System.Drawing.Point(0, 0);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(157, 48);
            this.button12.TabIndex = 24;
            this.button12.Text = "Greičiausio Nusileidimo metodas";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.Button12_Click);
            // 
            // U21
            // 
            this.U21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.U21.AutoSize = true;
            this.U21.Checked = true;
            this.U21.Location = new System.Drawing.Point(9, 54);
            this.U21.Name = "U21";
            this.U21.Size = new System.Drawing.Size(59, 21);
            this.U21.TabIndex = 25;
            this.U21.TabStop = true;
            this.U21.Text = "U2.1";
            this.U21.UseVisualStyleBackColor = true;
            // 
            // U22
            // 
            this.U22.AutoSize = true;
            this.U22.Location = new System.Drawing.Point(83, 54);
            this.U22.Name = "U22";
            this.U22.Size = new System.Drawing.Size(59, 21);
            this.U22.TabIndex = 26;
            this.U22.Text = "U2.2";
            this.U22.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.U21);
            this.panel1.Controls.Add(this.button12);
            this.panel1.Controls.Add(this.U22);
            this.panel1.Location = new System.Drawing.Point(838, 542);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(157, 84);
            this.panel1.TabIndex = 27;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1065, 1053);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radioZ2);
            this.Controls.Add(this.radioZ1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbAzimuth);
            this.Controls.Add(this.tbElevation);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trkAzimuth);
            this.Controls.Add(this.trkElevation);
            this.Controls.Add(this.PlotPanel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.trkElevation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkAzimuth)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Panel PlotPanel;
        private System.Windows.Forms.TrackBar trkElevation;
        private System.Windows.Forms.TrackBar trkAzimuth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbElevation;
        private System.Windows.Forms.TextBox tbAzimuth;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RadioButton radioZ1;
        private System.Windows.Forms.RadioButton radioZ2;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.RadioButton U21;
        private System.Windows.Forms.RadioButton U22;
        private System.Windows.Forms.Panel panel1;
    }
}

