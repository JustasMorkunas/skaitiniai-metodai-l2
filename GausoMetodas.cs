﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PracticalGuideCharts
{
    class GausoMetodas
    {

        private RichTextBox textOut;

        double[,] matrixA = new double[,] {
            {  1.0, -2.0,  3.0,  4.0 },
            {  1.0,  0.0, -1.0,  1.0 },
            {  2.0, -2.0,  2.0,  5.0 },
            { -7.0,  0.0,  3.0,  1.0 }
        };

        double[,] rezults = new double[,] {
            { 11.0 },
            { -4.0 },
            {  7.0 },
            {  2.0 }
        };

        // -----------------------------------------
        // Teorijos medziagoje esantys pavyzdziai
        // -----------------------------------------

        //double[,] matrixA = new Double[,] {
        //    {  1.0, 1.0,  1.0,  1.0 },
        //    {  1.0,  -1.0, -1.0,  1.0 },
        //    {  2.0, 1.0,  -1.0,  2.0 },
        //    {  3.0,  1.0,  2.0,  -1.0 }
        //};

        //double[,] rezults = new Double[,] {
        //    { 2.0 },
        //    { 0.0 },
        //    {  9.0 },
        //    {  7.0 }
        //};

        public GausoMetodas(RichTextBox textOut)
        {
            this.textOut = textOut;
        }

        public GausoMetodas(RichTextBox richText, double[,] matrix, double[,] rez)
        {
            textOut = richText;
            matrixA = matrix;
            rezults = rez;
        }

        public double[] getGauso()
        {
            try
            {
                double[,] matrix = tiesioginisEtapas();
                double[] res = atvirkstinisEtapas(matrix);
                textOut.AppendText("\nGauti rezultatai:\n");
                printResult(res);

                return res;
            }
            catch (OperationCanceledException e)
            {
                textOut.AppendText("\n" + e.Message);
            }
            return new double[0];
        }

        private double[,] tiesioginisEtapas()
        {
            int n = matrixA.GetLength(0);
            double[,] A = getMatrixWithRezults();

            printMatrix(A);
            for (int i = 0; i < n - 1; i++)
            {
                double pivot = A[i, i];

                if (pivot == 0)
                {
                    swapRows(i, ref A);
                }

                for (int j = i + 1; j < n; j++)
                {
                    double kons = A[j, i] / ((-1) * A[i, i]);

                    for (int k = i; k < n + 1; k++)
                    {
                        A[j, k] = A[j, k] + (kons * A[i, k]);
                    }
                }
                printMatrix(A);
            }

            return A;
        }

        private void swapRows(int currRow, ref double[,] matrix)
        {
            int maxRowIndex = -1;
            double maxPivot = 0;
            for (int i = currRow; i < matrix.GetLength(0); i++)
            {
                if (matrix[i, currRow] != 0)
                {
                    if (maxPivot < Math.Abs(matrix[i, currRow]))
                    {
                        maxRowIndex = i;
                        maxPivot = matrix[i, currRow];
                    }
                }
            }

            if (maxRowIndex == -1)
            {
                throw new OperationCanceledException("Matrica yra singuliari, todel sprendiniu nera arba yra be galo daug.");
            }

            double[] tempRow = getRow(matrix, currRow);
            setRow(ref matrix, currRow, getRow(matrix, maxRowIndex));
            setRow(ref matrix, maxRowIndex, tempRow);
        }

        private double[] atvirkstinisEtapas(double[,] matrix)
        {

            int n = matrix.GetLength(0);
            double[] ats = new double[n];

            // Jei paskutines eilutes paskutinis elementas lygus 0
            if (matrix[n - 1, n - 1] == 0)
            {
                throw new OperationCanceledException("Matrica yra singuliari, todel sprendiniu nera arba yra be galo daug.");
            }

            for (int i = n - 1; i >= 0; i--)
            {
                double dal = matrix[i, n];
                for (int j = i + 1; j < n; j++)
                {
                    dal = dal - (matrix[i, j] * ats[j]);
                }
                ats[i] = dal / matrix[i, i];
            }

            return ats;
        }


        private double[,] getMatrixWithRezults()
        {
            double[,] A = new double[matrixA.GetLength(0), matrixA.GetLength(1) + 1];

            for (int i = 0; i < matrixA.GetLength(0); i++)
            {
                for (int j = 0; j < matrixA.GetLength(1); j++)
                {
                    A[i, j] = matrixA[i, j];
                }
            }

            for (int i = 0; i < rezults.GetLength(0); i++)
            {
                A[i, matrixA.GetLength(1)] = rezults[i, 0];
            }

            return A;
        }

        private double[] getRow(double[,] matrix, int rowId)
        {
            double[] row = new double[matrix.GetLength(0) + 1];
            for (int i = 0; i < matrix.GetLength(0) + 1; i++)
            {
                row[i] = matrix[rowId, i];
            }
            return row;
        }

        private void setRow(ref double[,] matrix, int rowId, double[] row)
        {
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                matrix[rowId, i] = row[i];
            }
        }

        private void printMatrix(double[,] matrix)
        {
            textOut.AppendText("\n\nMatrica:\n");
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (j == matrix.GetLength(1) - 1)
                    {
                        textOut.AppendText(" | ");
                    }
                    textOut.AppendText(string.Format("{0,2} ", matrix[i, j]));
                }
                textOut.AppendText("\n");
            }
        }

        private void printResult(double[] res)
        {
            for (int i = 0; i < res.Length; i++)
            {
                textOut.AppendText(String.Format("x{0} = {1,0:f2}\n", i + 1, res[i]));
            }
        }

    }
}
