﻿
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PracticalGuideCharts
{
    class GreiciausioNusileidimoMetodas
    {
        public const string T1 = "T1";
        public const string T2 = "T2";
     
        RichTextBox textOutput;
        private string T;
        public GreiciausioNusileidimoMetodas(RichTextBox richTextBox, string T) {
            textOutput = richTextBox;
            this.T = T;
        }

        public void calculate() {
            switch (T) {
                case T1:
                    calculate1();
                    break;
                case T2:
                    calculate2();
                    break;
            }
        }

        public void calculate1() {
            List<Matrix<double>> startingPoints = new List<Matrix<double>>();
            startingPoints.Add(DenseMatrix.OfArray(new double[,] {
                { 3.0 },
                { -1.0 }
            }));

            startingPoints.Add(DenseMatrix.OfArray(new double[,] {
                { 3.0 },
                { -5.0 }
            }));

            startingPoints.Add(DenseMatrix.OfArray(new double[,] {
                { 0.0 },
                { -8.0 }
            }));

            startingPoints.Add(DenseMatrix.OfArray(new double[,] {
                { 2.0 },
                { 7.0 }
            }));

            int iterationCounter = 1;

            foreach (Matrix<double> startingPoint in startingPoints) {
                textOutput.AppendText("\n\n===========================================\n");
                textOutput.AppendText(String.Format("    Pradinis artinys {0}", iterationCounter));
                textOutput.AppendText("\n===========================================\n");
                calculate(startingPoint);
                iterationCounter++;
            }

        }

        public void calculate2() {
            Matrix<double> startingPoint = DenseMatrix.OfArray(new double[,] {
                { 2.0},
                { 3.0 },
                { 2.0 },
                { 4.0 }
            });

            calculate(startingPoint);
        }

        public void calculate(Matrix<double> startingPoint) {

            Matrix<double> functionParameters = startingPoint;
            int gradientRecalculations = 1;
            int iteration = 1;
            Matrix<double> results = Function(functionParameters);
            Matrix<double> gradient = this.calculateGradient(functionParameters);

            textOutput.AppendText(String.Format("x =\n {0}\n\n", functionParameters.ToMatrixString()));

            while (gradientRecalculations < Settings.MAX_GRADIENT_RECALCULATIONS && !checkAccuracy(results) && iteration < Settings.MAX_ITERATIONS) {
                Matrix<double> tempParameters = getNewParameters(functionParameters, gradient);

                if (compareResults(Function(tempParameters), results) == -1)
                {
                    functionParameters = tempParameters;
                    results = Function(functionParameters);
                }
                else {
                    gradient = calculateGradient(tempParameters);
                    gradientRecalculations++;
                }
                iteration++;
                printResults(gradientRecalculations, results);
            }

            textOutput.AppendText("Galutiniai artiniai: ");
            for (int i = 0; i < functionParameters.RowCount; i++) {
                textOutput.AppendText(String.Format("\nx{0} = {1,0:f2} ", i + 1, functionParameters[i,0]));
            }

        }

        private void printResults(int iteration, Matrix<double> results) {
            textOutput.AppendText(String.Format("{0,5} ", iteration));

            for (int i = 0; i < results.RowCount; i++) {
                textOutput.AppendText(String.Format("| F{0} = {1,0:f4}", i+1, results[i, 0]));
            }
            textOutput.AppendText("\n");
        }

        private Matrix<double> getNewParameters(Matrix<double> x, Matrix<double> gradient) {
            return x - (Settings.STEP_SIZE * gradient.Transpose());
        }

        private Matrix<double> Function(Matrix<double> x) {

            switch (T)
            {
                case T1:
                    return Function1(x);
                case T2:
                    return Function2(x);
            }
            throw new InvalidOperationException("Operation does not exist");
        }

        private Matrix<double> Function1(Matrix<double> x) {
            return DenseMatrix.OfArray(new double[,] {
                { Math.Pow(Math.Sin(x[0,0]/2), 3) + Math.Pow(Math.Cos(x[1,0]/2), 2) - 0.5 },
                { Math.Pow((x[1,0] - 3), 2) + Math.Pow(x[0,0], 2) + (x[0,0] * x[1,0]) - 4 }
            });
        }

        private Matrix<double> Function2(Matrix<double> x)
        {
            return DenseMatrix.OfArray(new double[,] {
                { (4 * x[0,0]) + (5 * x[1,0]) + (4*x[3,0]) + 13},
                { (3 * Math.Pow(x[0,0], 2)) + (4 * Math.Pow(x[3,0],2)) - 31},
                { (2 * Math.Pow(x[1,0], 3)) - (x[3,0] * x[1,0]) + (2 * Math.Pow(x[2,0], 2)) },
                { (4* x[2,0]) - (15 * x[1,0]) + (4 * x[3,0]) - 27 }
            });
        }


        private Matrix<double> calculateGradient(Matrix<double> x) {
            Matrix<double> partialDerivatives = this.calculatePartialDerivatives(x);
            Matrix<double> gradient = Function(x).Transpose() * calculatePartialDerivatives(x);
            return gradient/(gradient.L2Norm());
        }

        private Matrix<double> calculatePartialDerivatives(Matrix<double> x) {
            switch (T)
            {
                case T1:
                    return partialDerivative1(x);
                case T2:
                    return partialDerivative2(x);
            }
            throw new InvalidOperationException("Operation does not exist");
        }

        private Matrix<double> partialDerivative1(Matrix<double> x) {
            return DenseMatrix.OfArray(new double[,] {
                { 1.5 * Math.Pow(Math.Sin(0.5 * x[0,0]), 2) * Math.Cos(0.5 * x[0,0]), -0.5 * Math.Sin(x[1,0])},
                { 4.0 * x[0,0] * (Math.Pow(x[0,0], 2) - 3.0) + x[1,0], x[0,0] + (2.0 * x[1,0])}
            });
        }

        private Matrix<double> partialDerivative2(Matrix<double> x) {
            return DenseMatrix.OfArray(new double[,] {
                { 4.0, 5.0, 0.0, 4.0},
                { (6.0 * x[0,0]), 0.0, 0.0, (8.0 * x[3,0])},
                { 0.0, (6.0 * Math.Pow(x[1,0], 2) - x[3,0]), (-4.0 * x[2,0]), (-1.0 * x[1,0])},
                { 0.0, -15.0, 4.0, 4.0}
            });
        }

        private int compareResults(Matrix<double> m1, Matrix<double> m2) {
            int equalCounter = 0;
            int largerThenCounter = 0;
            int lessThanCounter = 0;

            for (int i = 0; i < m1.RowCount; i++) {
                switch (m1[i, 0].CompareTo(m2[i, 0])) {
                    case 0:
                        equalCounter++;
                        break;
                    case 1:
                        largerThenCounter++;
                        break;
                    case -1:
                        lessThanCounter++;
                        break;
                }
            }

            if (equalCounter > largerThenCounter && equalCounter > lessThanCounter)
                return 0;

            if (largerThenCounter >= equalCounter && largerThenCounter > lessThanCounter)
                return 1;

            return -1;
        }

        private bool checkAccuracy(Matrix<double> results) {
            for (int i = 0; i < results.RowCount; i++) {
                if (Math.Abs(results[i,0]) > Settings.MIN_ACCURACY) {
                    return false;
                }
            }

            return true;
        }

    }
}
