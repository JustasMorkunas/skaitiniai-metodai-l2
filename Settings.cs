﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticalGuideCharts
{
    class Settings
    {
        public static int MAX_GRADIENT_RECALCULATIONS = 30;

        public static int MAX_ITERATIONS = 100;

        public static double MIN_ACCURACY = 0.01;

        public static double STEP_SIZE = 0.1;


    }
}
